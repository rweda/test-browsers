const versions = require("./versions");
const fs = require("fs-extra");
const YAML = require("yamljs");

const ci = {

  image: "docker:latest",

  services: [
    "docker:dind",
  ],

  stages: [
    "lint",
    "build script images",
    "build browser images",
    "release",
  ],

  "gitlab-ci.yml up to date?": {
    stage: "lint",
    image: "mhart/alpine-node:7",
    script: [
      "npm install",
      "cp .gitlab-ci.yml .pushed.gitlab-ci.yml",
      "node ci.js",
      "echo \"The following command will fail if 'ci.js' has changed without rebuilding '.gitlab-ci.yml'.\"",
      "cmp .gitlab-ci.yml .pushed.gitlab-ci.yml",
    ],
  },

};

function lookupOSVersion(os, version) {
  for(const version_name in os.versions) {
    if(!os.versions.hasOwnProperty(version_name)) { continue; }
    const { names } = os.versions[version_name];
    if(names.indexOf(version) > -1) {
      return version_name;
    }
  }
  throw new ReferenceError(`Couldn't find version ${version}.`);
}

for(const pkg_name in versions.script_packages) {
  if(!versions.script_packages.hasOwnProperty(pkg_name)) { continue; }
  const script_package = versions.script_packages[pkg_name];
  for(const os_name in script_package.os) {
    if(!script_package.os.hasOwnProperty(os_name)) { continue; }
    for(const os_version_name of script_package.os[os_name]) {
      const os_version = lookupOSVersion(versions.os[os_name], os_version_name);

      ci[`build script package (${pkg_name})`] = {
        stage: "build script images",
        before_script: [
          "docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY_IMAGE",
          "apk add --no-cache nodejs-current nodejs-current-npm",
          "npm install",
        ],
        script: [
          `node ci/script_package.js setup --os "${os_name}" --os-version "${os_version}" "${pkg_name}"`,
          `node ci/script_package.js build "${pkg_name}" -t "temporary_image"`,
          `node ci/script_package.js upload --os "${os_name}" --os-version "${os_version}" "${pkg_name}" -t "temporary_image"`,
        ],
      };

    }
  }
}

for(const bundle_name in versions.browser_bundles) {
  if(!versions.browser_bundles.hasOwnProperty(bundle_name)) { continue; }
  const browser_bundle = versions.browser_bundles[bundle_name];
  for(const os_name in browser_bundle.os) {
    if(!browser_bundle.os.hasOwnProperty(os_name)) { continue; }
    for(const os_version_name of browser_bundle.os[os_name]) {
      const os_version = lookupOSVersion(versions.os[os_name], os_version_name);

      ci[`build browser bundle (${bundle_name})`] = {
        stage: "build browser images",
        before_script: [
          "docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY_IMAGE",
          "apk add --no-cache nodejs-current nodejs-current-npm",
          "npm install",
        ],
        script: [
          `node ci/browser_bundle.js setup --os "${os_name}" --os-version "${os_version}" "${bundle_name}"`,
          `node ci/browser_bundle.js build "${bundle_name}" -t "temporary_image"`,
          `node ci/browser_bundle.js test "${bundle_name}" --os "${os_name}" -t "temporary_image"`,
          `node ci/browser_bundle.js upload --os "${os_name}" --os-version "${os_version}" "${bundle_name}" -t "temporary_image"`,
        ],
      }
    };

  }
}

fs.writeFile(".gitlab-ci.yml", YAML.stringify(ci));
