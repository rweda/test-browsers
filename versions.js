const os = {

  ubuntu: {
    descriptive: "Ubuntu",
    tag_prefix: "ubuntu:",
    script_platform: "debian",
    default: true,
    versions: {

      "16.04": {
        latest: true,
        tag: "16.04",
        names: [ "16.04", "latest", "xenial" ],
      },

    },
  },

};

const scripts = {

  firefox: {
    source_prefix: "scripts/firefox_install_",
    names: [ "firefox", "ff" ],

    platforms: {

      debian: {
        source: "debian",
        out: "/usr/bin/local/firefox_install",
      },

    },

  },

};

const script_packages = {

  all: {
    default: true,
    scripts: [ "firefox" ],
    os: {
      ubuntu: [ "latest" ],
    },
  },

};

const browsers = {

  firefox: {
    names: [ "firefox", "ff" ],
    platforms: {

      debian: {
        script: (version) => `/usr/bin/local/firefox_install ${version}`,
        test: (version) => `/usr/local/lib/firefox-versions/${version}/firefox --version`,
        versions: {

          "54.0.1": {
            latest: true,
            version: "54.0.1",
            tags: [ "54.0.1", "latest", "54" ],
          },

        },
      },

    },
  },

};

const browser_bundles = {

  latest: {
    default: true,
    os: {
      ubuntu: [ "latest" ],
    },
    script_package: "all",
    browsers: {

      firefox: {
        versions: [ "latest" ],
      },

    },

  },

  most: {
    os: {
      ubuntu: [ "latest" ],
    },
    script_package: "all",
    browsers: {

      firefox: {
        versions: [ "54.0.1" ],
      },

    },
  },

};

module.exports = { os, scripts, script_packages, browsers, browser_bundles };
