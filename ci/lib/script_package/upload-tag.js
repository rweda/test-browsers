/**
 * Creates the full Docker registry path for this script package.
 * @param {String} script_package the name of the script package.
 * @param {String} os the name of the OS.
 * @param {String} version the version of the OS.
 * @param {Boolean} [release] `true` if building the latest release.  Otherwise tags with a build specifier
 *   (e.g. the branch name).
 * @return {String} the computed tag name.
*/
function tag(script_package, os, version, release) {
  let version_tag = [script_package, os, version].filter(a => [true, ""].indexOf(a) === -1).join("-");
  if(!release) {
    version_tag += "-" + (process.env.CI_COMMIT_REF_NAME || "local-build");
  }
  const full_tag = (process.env.CI_REGISTRY_IMAGE || "registry.gitlab.com/rweda/test-browsers") + "/install_scripts:" + version_tag;
  return full_tag;
}

module.exports = tag;
