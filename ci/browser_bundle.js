const cli = require("commander");
const fs = require("fs-extra");
const path = require("path");
const stripIndent = require("strip-indent");
const versions = require("../versions");
const { exec } = require("child-process-promise");
const express = require("express");
const script_package_tag = require("./lib/script_package/upload-tag");

function lookupBrowser(browser) {
  for(const browser_name in versions.browsers) {
    if(!versions.browsers.hasOwnProperty(browser_name)) { continue; }
    const { names } = versions.browsers[browser_name];
    if(names.indexOf(browser) > -1) {
      return browser_name;
    }
  }
  throw new ReferenceError(`Couldn't find browser ${browser}.`);
}

function lookupBrowserVersion(versions, name) {
  for(const version_name in versions) {
    if(!versions.hasOwnProperty(version_name)) { continue; }
    const { tags } = versions[version_name];
    if(tags.indexOf(name) > -1) {
      return version_name;
    }
  }
  throw new ReferenceError(`Couldn't find version ${name}.`);
}

function browserBundleDir(name) {
  return path.join(__dirname, "../", "dist", "browser-bundle", name);
}

cli
  .option("-t, --tag [tag]", "The Docker tag to attach to the built image.", "tmp-script-package")
  .option("--os [name]", "The name of the OS to use.")
  .option("--os-version [version]", "The version of the OS to use.", "latest");

cli
  .command("setup <bundle>")
  .description("Setup a directory ready to build the given browser bundle.")
  .action((name, opts) => {
    console.info(`Creating browser bundle '${name}' for ${cli.os} ${cli.osVersion}`);
    const dir = browserBundleDir(name);
    const browser_bundle = versions.browser_bundles[name];
    const os = versions.os[cli.os];
    const version = os.versions[cli.osVersion];

    // TODO: join script runs into a single `RUN` command.

    const browser_install_commands = [];

    for(let browser_name in browser_bundle.browsers) {
      if(!browser_bundle.browsers.hasOwnProperty(browser_name)) { continue; }
      browser_name = lookupBrowser(browser_name);
      const browser = versions.browsers[browser_name];
      if(!browser.platforms[os.script_platform]) {
        throw new Error(`Browser ${browser.names[0]} doesn't support platform ${os.script_platform}`);
      }
      const { script } = browser.platforms[os.script_platform];
      for(let browser_version of browser_bundle.browsers[browser_name].versions) {
        browser_version = lookupBrowserVersion(browser.platforms[os.script_platform].versions, browser_version);
        browser_install_commands.push(`RUN ${script(browser_version)}`);
      }
    }

    const browser_installs = browser_install_commands
      .join("\\\n      ");

    const dockerfile = stripIndent(`\
      FROM ${script_package_tag(browser_bundle.script_package, cli.os, cli.osVersion)}

      ${browser_installs}
    `);

    fs
      .ensureDir(dir)
      .then(() => fs.writeFile(path.join(dir, "Dockerfile"), dockerfile))
      .then(() => console.info(`write 'Dockerfile' for browser bundle ${name}`));
  });

cli
  .command("build <package>")
  .description("Run 'docker build' on the given package.")
  .action(name => {
    const dir = browserBundleDir(name);
    exec(`docker build -t "${cli.tag}" .`, { cwd: dir })
      .then(({ stderr, stdout }) => {
        console.log(stdout);
        console.log(stderr);
      })
      .catch(err => {
        if(err.stderr) {
          console.log(err.stdout);
          console.log(err.stderr);
        }
        console.error(err._stack || err);
        process.exit(1);
      });
  });

cli
  .command("test <package>")
  .description("Tests the packaged browser.")
  .action(name => {
    const browser_bundle = versions.browser_bundles[name];
    const os = versions.os[cli.os];

    const tests = [];
    for(let browser_name in browser_bundle.browsers) {
      if(!browser_bundle.browsers.hasOwnProperty(browser_name)) { continue; }
      browser_name = lookupBrowser(browser_name);
      const browser = versions.browsers[browser_name];
      const { test } = browser.platforms[os.script_platform];
      if(!test) { continue; }
      for(let browser_version of browser_bundle.browsers[browser_name].versions) {
        browser_version = lookupBrowserVersion(browser.platforms[os.script_platform].versions, browser_version);
        tests.push(exec(`docker run --rm "${cli.tag}" /bin/bash -c "${test(browser_version)}"`));
      }
    }
    Promise
      .all(tests)
      .then(() => {
        console.log("All tests passed successfully.");
      })
      .catch(err => {
        if(err.stderr) {
          console.log(err.stdout);
          console.log(err.stderr);
        }
        console.error(err._stack || err);
        process.exit(1);
      });
  });

cli
  .command("upload <package>")
  .description("Run 'docker push' for this image.")
  .option("--release", "If set, publishes with ':latest'.  Otherwise, publishes under the current Git branch.")
  .action((name, opts) => {
    let p = Promise.resolve();
    let tag = cli.tag;
    function uploadVersion(browser_bundle, os, version) {
      let version_tag = [browser_bundle, os, version].filter(a => [true, ""].indexOf(a) === -1).join("-");
      if(!opts.release) {
        version_tag += "-" + (process.env.CI_COMMIT_REF_NAME || "local-build");
      }
      const full_tag = (process.env.CI_REGISTRY_IMAGE || "registry.gitlab.com/rweda/test-browsers") + "/bundle:" + version_tag;
      p = p
        .then(() => exec(`docker tag "${tag}" "${full_tag}"`))
        .then(({ stderr, stdout }) => {
          console.log(stdout);
          console.log(stderr);
        })
        .then(() => tag = full_tag)
        .then(() => exec(`docker push "${tag}"`))
        .then(({ stderr, stdout }) => {
          console.log(stdout);
          console.log(stderr);
        });
    }
    const os = versions.os[cli.os];
    const os_names = os.latest ? [ cli.os, "" ] : [ cli.os ];
    const version = os.versions[cli.osVersion];
    const version_names = version.names;
    const bundle_names = [ name ];
    if(version.latest) { version_names.push(""); }
    for(const os_name of os_names) {
      for(const version_name of version_names) {
        for(const bundle_name of bundle_names) {
          uploadVersion(bundle_name, os_name, version_name);
        }
      }
    }
    p
      .then(() => console.log("Bundles uploaded."))
      .catch(err => {
        if(err.stderr) {
          console.log(err.stdout);
          console.log(err.stderr);
        }
        console.error(err._stack || err);
        process.exit(1);
      });
  });

cli.parse(process.argv);
