const cli = require("commander");
const fs = require("fs-extra");
const path = require("path");
const stripIndent = require("strip-indent");
const versions = require("../versions");
const { exec } = require("child-process-promise");
const express = require("express");

/**
 * Would be nice if 'docker build' fetched all scripts over curl in a single RUN statement, instead of multiple copies.
 * However, figuring out how to create and connect to a local server in GitLab CI is outside of scope.

 RUN apt-get update && apt-get install -y \\
     curl \\
   && \\
   ${fetch_scripts} && \\
   apt-get remove --purge -y \\
     curl \\
   && rm -rf /var/lib/apt/lists/*
*/
function lookupScript(script) {
  for(const script_name in versions.scripts) {
    if(!versions.scripts.hasOwnProperty(script_name)) { continue; }
    const { names } = versions.scripts[script_name];
    if(names.indexOf(script) > -1) {
      return script_name;
    }
  }
  throw new ReferenceError(`Couldn't find script ${script}.`);
}

function scriptPackageDirectory(name) {
  return path.join(__dirname, "../", "dist", "script-package", name);
}

cli
  .option("-t, --tag [tag]", "The Docker tag to attach to the built image.", "tmp-script-package")
  .option("--os [name]", "The name of the OS to use.")
  .option("--os-version [version]", "The version of the OS to use.", "latest");

cli
  .command("setup <package>")
  .description("Setup a directory ready to build the given script bundle.")
  .action((name, opts) => {
    console.info(`Creating script package '${name}' for ${cli.os} ${cli.osVersion}`);
    const dir = scriptPackageDirectory(name);
    const script_package = versions.script_packages[name];
    const os = versions.os[cli.os];
    const version = os.versions[cli.osVersion];
    // TODO: use different `fetch` functions depending on the platform
    //const fetch = (file, dest) => `curl -fL -o "${dest}" "http://localhost:64446/${file}"`;
    const fetch = (file, dest) => `COPY ${file} ${dest}`;
    const parse_scripts = script_package.scripts
      .map(lookupScript)
      .map(script_name => versions.scripts[script_name])
      .map(script => {
        const platform = script.platforms[os.script_platform];
        const src = `${script.source_prefix}${platform.source}`;
        const out = platform.out;
        return { src, out };
      });
    const fetch_scripts = parse_scripts
      .map(({ src, out }) => fetch(src, out))
      .join(" && \\\n          ");
    // TODO: Add any required installs depending on the script.
    const dockerfile = stripIndent(`\
      FROM ${os.tag_prefix || ""}${version.tag}

      ${fetch_scripts}
    `);
    const scriptCopies = parse_scripts
      .map(({ src }) => {
        return fs
          .ensureDir(path.join(dir, path.dirname(src)))
          .then(() => fs.copy(path.join(__dirname, "../", src), path.join(dir, src)));
      });
    fs
      .ensureDir(dir)
      .then(() => Promise.all([
        fs.writeFile(path.join(dir, "Dockerfile"), dockerfile),
        ...scriptCopies,
      ]))
      .then(() => console.info(`wrote 'Dockerfile' for script package ${name}`));
  });

cli
  .command("build <package>")
  .description("Run 'docker build' on the given package.")
  .action(name => {
    const dir = scriptPackageDirectory(name);
    /*const app = express();
    app.use(express.static(path.join(__dirname, "../")));
    app.listen(64446);*/
    exec(`docker build -t "${cli.tag}" .`, { cwd: dir })
      .then(({ stderr, stdout }) => {
        console.log(stdout);
        console.log(stderr);
      });
  });

cli
  .command("upload <package>")
  .description("Run 'docker push' for this image.")
  .option("--release", "If set, publishes with ':latest'.  Otherwise, publishes under the current Git branch.")
  .action((name, opts) => {
    let p = Promise.resolve();
    let tag = cli.tag;
    function uploadVersion(script_package, os, version) {
      let version_tag = [script_package, os, version].filter(a => [true, ""].indexOf(a) === -1).join("-");
      if(!opts.release) {
        version_tag += "-" + (process.env.CI_COMMIT_REF_NAME || "local-build");
      }
      const full_tag = (process.env.CI_REGISTRY_IMAGE || "registry.gitlab.com/rweda/test-browsers") + "/install_scripts:" + version_tag;
      p = p
        .then(() => exec(`docker tag "${tag}" "${full_tag}"`))
        .then(({ stderr, stdout }) => {
          console.log(stdout);
          console.log(stderr);
        })
        .then(() => tag = full_tag)
        .then(() => exec(`docker push "${tag}"`))
        .then(({ stderr, stdout }) => {
          console.log(stdout);
          console.log(stderr);
        });
    }
    const os = versions.os[cli.os];
    const os_names = os.latest ? [ cli.os, "" ] : [ cli.os ];
    const version = os.versions[cli.osVersion];
    const version_names = version.names;
    const package_names = [ name ];
    if(version.latest) { version_names.push(""); }
    for(const os_name of os_names) {
      for(const version_name of version_names) {
        for(const package_name of package_names) {
          uploadVersion(package_name, os_name, version_name);
        }
      }
    }
    p
      .then(() => console.log("Packages uploaded."))
      .catch(err => {
        if(err.stderr) {
          console.log(err.stdout);
          console.log(err.stderr);
        }
        console.error(err._stack || err);
        process.exit(1);
      });
  });

cli.parse(process.argv);
